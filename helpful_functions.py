'''
File with helpful functions for the interface of BLonD and Xsuite

:Author: **Birk Emil Karlsen-Baeck**
'''

# Imports
import numpy as np
from scipy.optimize import bisect
from scipy.interpolate import interp1d
from scipy.constants import c as clight
from blond.input_parameters.ring_options import load_data, RingOptions

def blond_coords_to_xsuite_coords(dt, dE, beta0, energy0, phi_s: float = 0, omega_rf: float = 1):
    r'''
    Coordinate transformation from BLonD to Xsuite
    :input dt, dE: either numpy-array (or single variable)
    :params beta0, energy0, phi_s, omega_rf as single variables

    :return zeta, ptau as numpy-arrays (or single variable)
    '''
    ptau = dE / (beta0 * energy0)
    zeta = -(dt - phi_s / omega_rf) * beta0 * clight
    return zeta, ptau


def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return array[idx], idx


def fetch_momentum_program(fname, C, particle_mass, scale_data=1e9, target_momentum=None):
    r'''
    Fetching momentum program from a CSV-file.

    :param fname: name of momentum program file
    :param C: circumferenc of the machine
    :param particle_mass: mass of particles in accelerator
    :param scale_data: option to scale data
    :return: numpy-array with turn-by-turn momentum value
    '''

    time, data = load_data(fname, ignore=2, delimiter=',')
    data *= scale_data

    if target_momentum is not None:
        data, time = cut_momentum_program(target_momentum, data, time)

    ring_option = RingOptions()
    time_interp, momentum = ring_option.preprocess(mass=particle_mass, circumference=C,
                                                   time=time, momentum=data)

    return np.ascontiguousarray(momentum)


def cut_momentum_program(target_momentum, momentum_program, program_time):
    r'''
    Cut the momentum program short at a chosen momentum value.

    :param target_momentum: Maximum momentum reached
    :param momentum_program: Original momentum program
    :param program_time: Original momentum program in time
    :return: new momentum program and time
    '''

    data_val, data_ind = find_nearest(momentum_program - target_momentum, 0)
    mom_func = interp1d(program_time[data_ind - 3: data_ind + 3],
                        momentum_program[data_ind - 3: data_ind + 3] - target_momentum)

    target_time = bisect(mom_func, program_time[data_ind - 1], program_time[data_ind + 1])

    if data_val < target_momentum:
        momentum_program = np.concatenate((momentum_program[:data_ind], np.array([target_momentum])))
        program_time = np.concatenate((program_time[:data_ind], np.array([target_time])))
    else:
        momentum_program = np.concatenate((momentum_program[:data_ind - 1], np.array([target_momentum])))
        program_time = np.concatenate((program_time[:data_ind - 1], np.array([target_time])))

    return momentum_program, program_time

def _compare_props(ring, tw, line, rfstation, V, cavity_name, i = 0):
    r'''
    Asserts certain properties in the ring and line elements and compares if they are the same.
    Can be used as a quick check to see if initial coordinates in interface simulation are the same.
    '''
    
    # Compare properties of the BLonD ring with Xsuite
    assert ring.ring_circumference == tw.circumference
    assert ring.alpha_0[0,i] == tw.momentum_compaction_factor
    assert ring.eta_0[0,i] == tw.slip_factor
    assert ring.momentum[0,i] == line.particle_ref.p0c[0]
    assert ring.beta[0,i] == line.particle_ref.beta0
    assert np.isclose(ring.gamma[0,i], line.particle_ref.gamma0, rtol=1e-15, atol=0) # not exactly the same
    assert ring.energy[0,i] == line.particle_ref.energy0
    assert np.isclose(ring.t_rev[i], tw.T_rev0, rtol= 1e-15, atol= 0) # not exactly the same

    # Compare the properties of the BLonD RF station
    assert rfstation.charge == line.particle_ref.charge
    assert rfstation.voltage[0,i] == V
    assert rfstation.phi_s[i] == line[cavity_name].lag / 180 * np.pi
    assert 1/rfstation.t_rf[0,i] == line[cavity_name].frequency
    assert rfstation.harmonic[0,i] == line[cavity_name].frequency * tw.T_rev0
    assert rfstation.Q_s[i] == np.sqrt(line[cavity_name].frequency * tw.T_rev0 * np.abs(line.particle_ref.charge) * V * np.abs(tw.slip_factor * np.cos(line[cavity_name].lag / 180 * np.pi)) / (2 * np.pi * line.particle_ref.beta0**2 * line.particle_ref.energy0 ))
