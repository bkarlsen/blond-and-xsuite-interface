# BLonD and Xsuite Interface
This code serves as an interface between the 2D longitudinal tracking code BLonD and the 6D tracking code Xsuite.

## Description
The interface can be imported from xsuite_blond_interface.py. Furthermore, helpful_functions.py contains some extra 
functions. Example simulations of the LHC and the PSB can be found in the ```__EXAMPLES``` folder along with input
files for such simulations. 

For a more comprehensive look at the different benchmarks that were performed for the interface have a look at the
```benchmarks``` branch. A detailed report can also soon be found written by T. A. van Rijswijk, who performed the
benchmarks and fine-tuned the interface.

## Installation
For the interface to work both Xsuite and BLonD needs to be installed. 
For installation follow the installation of Xsuite [xsuite.readthedocs.io](https://xsuite.readthedocs.io/en/latest/installation.html) as well as the installation of 
BLonD [gitlab.cern.ch/blond/BLonD](https://gitlab.cern.ch/blond/BLonD).

## Authors
- Birk Emil Karlsen-Baeck
- Thom Arnoldus van Rijswijk
- Helga Timko

## Project status
The single particle cases have now been benchmarked as well as single bunch cases without intensity effects.
In future, optimizations will be done and features will be expanded.